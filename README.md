# Cloud Storage 連線方式

在使用Cloud Storage前，我們需要先完成登入程序。以下是一種利用SDK進行連線的方法。需注意的是，由於資安的考量，不建議使用Json檔案的方式進行登入。

## SDK安裝
首先，我們需要安裝Cloud SDK。你可以在此[連結](https://cloud.google.com/sdk?hl=zh-tw)下載與安裝。安裝完成後，我們可以利用Python進行相關的操作，相關的Python操作文件可以參照[此處](https://cloud.google.com/python/docs/reference?hl=zh-cn)。

## 登入指令
完成SDK安裝後，請在你的命令列介面輸入以下指令：
```
gcloud auth application-default login
```
輸入指令後，會彈出Google的登入視窗，請依照指示進行登入。雖然這種登入方式具有一定的有效期限，但一般來說相當長，可以放心使用。更多相關的資訊可以參考[登入指令](https://cloud.google.com/docs/authentication/provide-credentials-adc?hl=zh-cn#local-dev)。

## 連線
完成登入後，我們可以透過以下的Python程式碼進行連線測試：
```python
from google.cloud import storage

storage_client = storage.Client(project=project_id) # 連線
buckets = storage_client.list_buckets() # 取得所有儲存桶的清單
bucket = client.get_bucket(bucket_name) # 取得特定的儲存桶
```
需要注意的是，你的帳號必須具有該儲存桶或物件的存取權限，否則將無法正常存取。

## 權限
Cloud Storage採取資源層次的管理方式。在這種結構中，儲存桶(Bucket)中包含與專案(Projects)相關的物件(Objects)，並且將這些物件與組織(Organization)綁定。你也可以使用資料夾(Folders)來進一步區分專案資源。組織政策(Org Policies)則可以在組織、資料夾或專案的層級進行設定，以強制特定的行為。

至於授予儲存桶和物件的存取權限，Cloud Storage提供了兩種方式：Cloud IAM與存取控制列表（Access Control Lists, ACL）。使用者只需要在這兩者之一獲得權限即可存取資源。

ACL具有物件級的特性，可以對單一物件授予存取權限。然而，隨著儲存桶中物件數量的增加，單一物件的ACL管理成本也會增加，進而增加了評估單一儲存桶中所有物件安全性的困難度。你可以想像，當必須檢查數百萬個物件來確認單一用戶是否具有正確的存取權限時，將會是多麼艱巨的任務。